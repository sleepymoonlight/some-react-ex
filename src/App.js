import React, {Component} from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

// import logo from './logo.svg';
import './App.css';

import TodoList from "./components/TodoList/TodoList";
import rootReducers from './reducers';
import logger from './middlewares/logger.js'


const store = createStore(rootReducers, applyMiddleware(thunk, logger));

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <div className="App">
                    {/*<header className="App-header">*/}
                        {/*<img src={logo} className="App-logo" alt="logo"/>*/}
                        {/*<h1 className="App-title">Welcome to React</h1>*/}
                    {/*</header>*/}
                    {/*<p className="App-intro">*/}
                        {/*To get started, edit <code>src/App.js</code> and save to reload.*/}
                    {/*</p>*/}
                    <TodoList/>
                </div>
            </Provider>
        );
    }
}

// class TimerComponent extends React.Component {
//
//     state = {
//         date: new Date()
//     };
//
//     componentDidMount () {
//         this.timer = setInterval( () => {
//             this.setState({date: new Date()});
//         }, 1000);
//     }
//
//     componentWillUnmount() {
//         clearInterval(this.timer);
//     }
//
//     render () {
//
//         return<div>{this.state.date.toString()}</div>
//
//     }
// }

// const ListItem = props => (<div>{this.props.name}</div>);
//
// class UserList extends React.Component {
//     state = {
//         data: []
//     };
//
//     componentDidMount() {
//         this.receiveData();
//     }
//
//     receiveData() {
//         fetch(url)
//             .then((res) => res.json())
//             .then((data) => this.setState({data, isLoading: false}));
//
//     }
//
//     render() {
//         const list = this.state.data.map(dataItem => (<ListItem name={{dataItem.username} key={dataItem.id} />))
//
//             const whatToRender = this.state.isLoading ? <div>Load</div>
//             return <React.Fragment>{whatToRender}</React.Fragment>
//
//     }
// }


export default App;
